<?php	
	@ini_set('expose_php', 'off');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneda Renta CLP Fondo de Inversión.</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.1.6/sumoselect.min.css">
    <link rel="stylesheet" href="assets/fonts/Hatton.css">
    <link rel="stylesheet" href="assets/css/main.css?v=7">
    <link rel="icon" type="image/png" href="favicon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P9QH2HS');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9QH2HS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <nav class="navbar navbar-light">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="/assets/img/logo-moneda.png" alt="">
            </a>
            <!--<a href="#">
            <img src="/assets/img/menu-icon.png" alt="">
            </a>-->
        </div>
    </nav>
    <section class="intro">
        <div class="container d-flex h-100">
            <div class="intro_content align-self-center" data-aos="fade-right">
                <h2>¿Cuál es el fondo de Renta Fija <br/>
                    más rentable de Chile? </h2>
                <span></span>
                <div class="d-flex align-items-center">
                <h1><strong>Moneda Renta CLP</strong> <br/>
                    de Moneda Asset <br/>
                    Management</h1>
                <img src="/assets/img/img-home.png" alt="Moneda" class="d-none d-md-block d-lg-block" style="height: 90px">
                </div>
                <img src="/assets/img/img-home.png" alt="Moneda" class="d-block d-md-none d-lg-none" style="height: 90px">
                    
            </div>
        </div>
        <a href="#invierte" class="goto">
            <img src="/assets/img/circulo-naranjo-flecha.png" alt="Moneda" class="img-fluid">
        </a>
    </section>


    <section id="invierte" class="section-2">
        <div class="container">
            <div class="row py-4">
                <div class="col-lg-5">
                    <img src="/assets/img/img-rentabilidad-1.png" alt="Moneda" class="img-fluid mt-4" style="width:300px">
                </div>
                <div class="col-lg-7">
                    <img src="/assets/img/img-rentabilidad-2.png" alt="Moneda" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row titles">
                <div class="col-lg-6" data-aos="fade-right">
                    <h2 class="text-end">Invierte hoy en<br/> tu futuro con
                    <span>Moneda Renta CLP</span></h2>
                </div>
                <div class="col-lg-6">
                    <p data-aos="fade-left">Una alternativa de inversión sólida, confiable y accesible con 9 años de trayectoria que busca las mejores oportunidades de Renta Fija en Chile y en el extranjero, administrada por <b>Moneda Asset Management.</b> </p>
                </div>
            </div>

            <div class="row bloque" data-aos="fade-up">
                <div class="col-lg-5 d-flex align-items-center justify-content-end">
                    <h3>Más<br/> Rentable</h3>
                </div>
                <div class="col-lg-1 position-relative">
                    <span></span>
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <p>Invierte en el Fondo con mejor rentabilidad del mercado de Renta Fija chilena, con 14,6% de rentabilidad el 2021.</p>
                </div>
            </div>

            <div class="row bloque" data-aos="fade-up">
                <div class="col-lg-5 d-flex align-items-center justify-content-end">
                    <h3>DIVERSIFICADO</h3>
                </div>
                <div class="col-lg-1 position-relative">
                    <span></span>
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <p>Diversifica tus ahorros en una cartera compuesta por más de 100 empresas chilenas y extranjeras de diferentes sectores de la economía. </p>
                </div>
            </div>

            <div class="row bloque" data-aos="fade-up">
                <div class="col-lg-5 d-flex align-items-center justify-content-end">
                    <h3>MÁS <br/>GRANDE</h3>
                </div>
                <div class="col-lg-1 position-relative">
                    <span></span>
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <p>Accede a un instrumento robusto, el fondo más grande del mercado de Renta Fija local con más de USD 340 MM de activos bajo administración.</p>
                </div>
            </div>

            <div class="row bloque" data-aos="fade-up">
                <div class="col-lg-5 d-flex align-items-center justify-content-end">
                    <h3>CONSISTENTE</h3>
                </div>
                <div class="col-lg-1 position-relative">
                    <span></span>
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <p>Sé parte de una estrategia sólida, con 9 años de excelente trayectoria: 9,1% de rentabilidad anual compuesta. </p>
                </div>
            </div>

            <div class="row bloque" data-aos="fade-up">
                <div class="col-lg-5 d-flex align-items-center justify-content-end">
                    <h3>EQUIPO <br/>DE EXPERTOS</h3>
                </div>
                <div class="col-lg-1 position-relative">
                    <span></span>
                </div>
                <div class="col-lg-6 d-flex align-items-center">
                    <p>Confía tus recursos a Moneda; un equipo de más de 25 profesionales expertos, innovadores, dedicados a buscar las mejores oportunidades de inversión. </p>
                </div>
            </div>
            
        </div>
    </section>

    <section class="section-7 section-7-a">
        <div class="container">
            <div class="block block-2" data-aos="fade-down">
                <div class="row">
                    <div class="col-lg-8 d-flex justify-content-center align-items-center">
                        <p>Invierte en una de las mejores alternativas de <strong>Renta Fija</strong> de alto rendimiento en pesos en los mercados públicos y privados de Chile y el extranjero, a través de tu Corredora de Bolsa: <strong>CFIMRCLPR</strong></p>
                    </div>
                    <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <a href="#contact" class="btn">
                            Invierte ahora
                        </a>
                    </div>
                </div>
            </div>
            <div class="steps">
                <h2>¿Cómo lo hacemos?</h2>
                <div class="step d-flex align-items-center">
                    <img src="/assets/img/step1.png" alt="¿Cómo lo hacemos?">
                    <p>Contamos con la flexibilidad necesaria para invertir tanto en Chile como en el extranjero, en Bonos y en financiamientos que nosotros mismos estructuramos.</p>
                </div>
                <div class="step d-flex align-items-center">
                    <img src="/assets/img/step-2.png" alt="¿Cómo lo hacemos?">
                    <p>Nos enfocamos en entender los distintos mercados analizando las alternativas de inversión, visitando a las compañías para conocerlas en profundidad y ofrecemos estructuras flexibles.</p>
                </div>
                <div class="step d-flex align-items-center">
                    <img src="/assets/img/step-3.png" alt="¿Cómo lo hacemos?">
                    <p>Excelencia y rapidez son el sello del equipo Moneda.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="section-3">
        <div class="cifras">
            <div class="container">
                <div class="row g-0">
                    <div class="col-12 text-center">
                        <h2 data-aos="fade-up">Moneda Renta CLP en cifras.</h2>
                        <p data-aos="fade-down">Nuestras cifras así lo demuestran: Moneda Renta CLP es el fondo más rentable de Renta Fija chilena, con una trayectoria sólida y consistente, con retornos positivos desde su creación.</p>
                    </div>
                    <div class="row g-0 flex-lg-row-reverse">
                        <div class="col-lg cifra especial" data-aos-delay="200">
                            2021
                            <span>14,6%</span>
                        </div>                    
                        <div class="col-lg cifra" data-aos-delay="400">
                            2020
                            <span>0,2%</span>
                        </div>
                        <div class="col-lg cifra" data-aos-delay="600">
                            2019
                            <span>1,4%</span>
                        </div>
                        <div class="col-lg cifra" data-aos-delay="800">
                            2018
                            <span>8,1%</span>
                        </div>
                        <div class="col-lg cifra" data-aos-delay="1000">
                            2017
                            <span>12,2%</span>
                        </div>
                        <div class="col-lg cifra" data-aos-delay="1200">
                            2016
                            <span>16,6%</span>
                        </div>
                        <div class="col-lg cifra" data-aos-delay="1400">
                            2015
                            <span>5,6%</span>
                        </div>
                        <div class="col-lg cifra" data-aos-delay="1600">
                            2014
                            <span>14,3%</span>
                        </div>
                        <div class="col-lg cifra" data-aos-delay="1800">
                            2013
                            <span>10,2%</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <a href="#conoce"><img src="/assets/img/circulo-azul-flecha.png" alt="Moneda" class="img-fluid flecha"></a>
    </section>  

    <section id="conoce" class="section-4">
        <div class="container">
            <h2 data-aos="fade-up">¿Cuánto tendrías hoy* si hubieras invertido en Moneda Renta CLP?</h2>
            <div class="invierte" data-aos="fade-down">
                <input type="text" placeholder="Ingresa tu monto" id="monto" class="inputmonto" onkeyup="this.value=this.value.replace(/[^\d]/,'')" maxlength="9">
            </div>

            <div class="row text-center timeline g-0 d-block d-md-none d-lg-none" data-aos="fade-up" data-aos-delay="500">

                <div class="col time">
                    <span class="actual calc2021">$0</span>
                    <div class="dot"></div>
                    <div class="year">2021</div>
                </div>

                <div class="col time">
                    <span class="calc2020">$0</span>
                    <div class="dot"></div>
                    <div class="year">2020</div>
                </div>
                <div class="col time">
                    <span class="calc2019">$0</span>
                    <div class="dot"></div>
                    <div class="year">2019</div>
                </div>
                <div class="col time">
                    <span class="calc2018">$0</span>
                    <div class="dot"></div>
                    <div class="year">2018</div>
                </div>
                <div class="col time">
                    <span class="calc2017">$0</span>
                    <div class="dot"></div>
                    <div class="year">2017</div>
                </div>
                <div class="col time">
                    <span class="calc2016">$0</span>
                    <div class="dot"></div>
                    <div class="year">2016</div>
                </div>
                <div class="col time">
                    <span class="calc2015">$0</span>
                    <div class="dot"></div>
                    <div class="year">2015</div>
                </div>
                <div class="col time">
                    <span class="calc2014">$0</span>
                    <div class="dot"></div>
                    <div class="year">2014</div>
                </div>
                <div class="col time">
                    <span class="calc2013">$0</span>
                    <div class="dot"></div>
                    <div class="year">2013</div>
                </div>
                
            </div>

            <div class="row text-center timeline g-0 d-none d-md-flex d-lg-flex" data-aos="fade-up" data-aos-delay="500">
            
            <div class="col time">
                    <span class="calc2013">$0</span>
                    <div class="dot"></div>
                    <div class="year">2013</div>
                </div>
            <div class="col time">
                    <span class="calc2014">$0</span>
                    <div class="dot"></div>
                    <div class="year">2014</div>
                </div>
            <div class="col time">
                    <span class="calc2015">$0</span>
                    <div class="dot"></div>
                    <div class="year">2015</div>
                </div>
            <div class="col time">
                    <span class="calc2016">$0</span>
                    <div class="dot"></div>
                    <div class="year">2016</div>
                </div>
            
            <div class="col time">
                    <span class="calc2017">$0</span>
                    <div class="dot"></div>
                    <div class="year">2017</div>
                </div>
            <div class="col time">
                    <span class="calc2018">$0</span>
                    <div class="dot"></div>
                    <div class="year">2018</div>
                </div>
            <div class="col time">
                    <span class="calc2019">$0</span>
                    <div class="dot"></div>
                    <div class="year">2019</div>
                </div>
                <div class="col time">
                    <span class="calc2020">$0</span>
                    <div class="dot"></div>
                    <div class="year">2020</div>
                </div>
                <div class="col time">
                    <span class="actual calc2021">$0</span>
                    <div class="dot"></div>
                    <div class="year">2021</div>
                </div>

                
            </div>
            <p><small>* Rentabilidad hasta el 31 de diciembre del 2021</small></p>   
        </div>
    </section>

    <section class="section-6">
        <div class="container">
            <h2 class="text-center" data-aos="fade-down">¿En qué invierte Moneda Renta CLP?</h2>
            <h3 class="text-center" data-aos="fade-down" data-aos-delay="100">Tenemos una cartera diversificada en 4 tipos de activos:</h3>
            <div class="row my-5">
                <div class="col-lg-12">
                    <span></span>
                </div>
                <div class="col-lg-6">
                    <img src="/assets/img/grafico-1.png" alt="Moneda" class="img-fluid" data-aos="fade-right">
                </div>
                <div class="col-lg-6">
                    <ul data-aos="fade-left">
                        <li>
                            Bonos emitidos en Chile
                        </li>
                        <li>
                            Deuda Privada: créditos sindicados,
                            créditos bilaterales, mutuos hipotecarios,
                            securitizaciones, capital preferente,
                            facturas, etc.
                        </li>
                        <li>
                            Bonos en mercados extranjeros
                        </li>
                        <li>
                            Situaciones Especiales,
                            reestructuraciones de deuda
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="section-7">
        <div class="container">
            <div class="block" data-aos="fade-down">
                <div class="row">
                    <div class="col-lg-8 d-flex justify-content-center align-items-center">
                        <p>Invierte en una de las mejores alternativas de <strong>Renta Fija</strong> de alto rendimiento en pesos en los mercados públicos y privados de Chile y el extranjero, a través de tu Corredora de Bolsa: <strong>CFIMRCLPR</strong></p>
                    </div>
                    <div class="col-lg-4 d-flex justify-content-center align-items-center">
                        <a href="#contact" class="btn">
                            Invierte ahora
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-8">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 pe-5 infomobile" data-aos="fade-left">
                    <img src="/assets/img/circulos-1.png" alt="Moneda" class="circulos">
                    <p>Ofrece un rendimiento substancialmente superior a otras alternativas tradicionales de Renta Fija chilena.</p>
                    <p><strong>¡Comprúebalo tú mismo!</strong></p>
                </div>
                <div class="col-lg-6" data-aos="fade-right">
                    <h3>RENDIMIENTO DEL FONDO MONEDA Renta CLP FRENTE A OTRAS ALTERNATIVAS DE RENTA FIJA</h3>
                    <img src="/assets/img/grafico-2.png" alt="Moneda" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section class="section-12">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 data-aos="fade-left">Invertir con <br/>
                        nosotros, es simple.</h2>
                </div>
                <div class="col-lg-6 offset-lg-5 d-flex">
                    <div class="contenido" data-aos="fade-down">
                        <img src="/assets/img/circulos-3.png" alt="Moneda">
                    <p>Invierte en Moneda Renta CLP a través de tu corredor de bolsa, solicítalo como <strong>CFIMRCLPR</strong>
                    </p>
                    </div>
                </div>
            </div>
            <div class="row g-5">
                <div class="col-lg-3 como" data-aos="fade-up">
                    <img src="/assets/img/como-1.png" alt="Invierte en Moneda Renta CLP" class="img-fluid">
                    <h4 class="text-center">Con sólo $13.400*</h4>
                    <p>A través de cualquier corredor de bolsa podrás acceder a nuestro fondo. Solicítalo por: CFIMRCLPR</p>
                </div>
                <div class="col-lg-3 como" data-aos="fade-up">
                    <img src="/assets/img/como-2.png" alt="Invierte en Moneda Renta CLP" class="img-fluid">
                    <h4 class="text-center">Completa accesibilidad</h4>
                    <p>Moneda Renta CLP posee una alta liquidez en el mercado secundario de la Bolsa de Santiago, lo que te permitirá entrar y salir del fondo cuando quieras.</p>
                </div>
                <div class="col-lg-3 como" data-aos="fade-up">
                    <img src="/assets/img/como-3.png" alt="Invierte en Moneda Renta CLP" class="img-fluid">
                    <h4 class="text-center">Obtén liquidez trimestral</h4>
                    <p>Reparto trimestral de dividendos, con una política de reparto de 1% trimestral.</p>
                </div>
                <div class="col-lg-3 como" data-aos="fade-up">
                    <img src="/assets/img/como-4.png" alt="Invierte en Moneda Renta CLP" class="img-fluid">
                    <h4 class="text-center">Beneficio Tributario</h4>
                    <p>A la Ganancia de Capital – 107 LIR.</p>
                </div>
                <div class="col-12 mt-5">
                    <p class="mt-5"><small>*Precio referencial de la última transacción en la Bolsa de Comercio de Santiago al 19/01/2022</small></p>
                </div>
            </div>
        </div>
    </section>

    <section class="section-12 section-12-b">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="text-center mb-5" data-aos="fade-down">Si no tienes un corredor de bolsa, conoce con quienes podrías invertir:</h4>
                </div>
                
                <div class="col-lg-3">
                    <div class="tarjeta" data-aos-delay="600">
                        <div class="img align-items-center">
                            <img src="/assets/img/banchile.png" alt="Moneda" class="img-fluid">
                        </div>
                        
                        <p>La compañía de inversiones líder del país. Por más de 40 años liderando el mercado, con un fuerte foco en la digitalización, a través de productos financieros con herramientas tecnológicas diseñadas especialmente para cada uno de los perfiles de los inversionistas y sus metas de inversión.</p>
                        <div class="foot">
                            
                            <a href="https://ww2.banchileinversiones.cl" target="_blank">https://ww2.banchileinversiones.cl</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="tarjeta" data-aos-delay="1200">
                        <div class="img align-items-center">
                            <img src="/assets/img/sura.png" alt="Moneda" class="img-fluid">
                        </div>
                        <p>Empresa líder en Latinoamérica dedicada a la asesoría y administración de inversiones, con más de 40 años en Chile, modelo único de arquitectura abierta en productos, y la mayor oferta en Chile para hacer APV.</p>
                        <div class="foot">
                            
                            <a href="https://inversiones.sura.cl" target="_blank">https://inversiones.sura.cl</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="tarjeta" data-aos-delay="900">
                        <div class="img align-items-center">
                            <img src="/assets/img/sherpa.png" alt="Moneda" class="img-fluid">
                        </div>
                        <p>Somos la mayor comunidad de asesores financieros independientes del país. En Sherpa contamos con la más amplia oferta de productos de inversión, lo que permite a nuestros clientes, soluciones a la medida, en la institución que ellos elijan.</p>
                        <div class="foot">
                            
                            <a href="https://www.sherpawmc.cl/" target="_blank">https://www.sherpawmc.cl/</a>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3">
                    <div class="tarjeta" data-aos-delay="300">
                        <div class="img align-items-center">
                            <img src="/assets/img/add-wise.png" alt="Moneda" class="img-fluid">
                        </div>
                        
                        <p>Uno de los principales multi family office chilenos. Pioneros en la industria e impulsores del modelo de arquitectura abierta en nuestro país. Fundado el año 2007 por ocho de sus ejecutivos que están directamente involucrados en la asesoría a clientes privados, empresas locales y extranjeras.</p>
                        <div class="foot">
                            
                            <a href="https://www.addwise.cl" target="_blank">https://www.addwise.cl</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-13">
        <div class="container bg-gris">
            <form action="" method="post" id="contact" data-validate>
                <fieldset id="fieldset">
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 data-aos="fade-left">O si quieres te contactamos</h2>
                        </div>
                        <div class="col-lg-4" data-aos="fade-right">
                            <img src="/assets/img/img-1.png" alt="" class="img-fluid">
                            <h3>Moneda Renta CLP<br/>
                                Fondo de Inversión.</h3>
                        </div>
                        <div class="col-lg-5" data-aos="fade-up" data-aos-delay="200">
                            <label for="name" class="form-label">Nombre</label>
                            <input type="text" class="form-control" placeholder="Nombre" id="name" name="name" required>
                            
                            <label for="lastname" for="lastname" class="form-label">Apellido</label>
                            <input type="text" class="form-control" placeholder="Apellido" id="lastname" name="lastname" required>

                            <label for="phone" class="form-label">Teléfono</label>
                            <input type="text" class="form-control" placeholder="" id="phone" name="phone" placeholder="+569 123456789" required>

                            <label for="email" class="form-label">Correo electrónico</label>
                            <input type="email" class="form-control" placeholder="ejemplo@mail.com" name="email" id="email" required>

                            <label for="distributor" class="form-label d-none">¿Qué distribuidor quieres que te contacte?</label>                            
                            <select name="" class="form-select d-none" id="distributor" name="distributor" required>
                                <option value="SIN INFO" selected></option>
                                <option value="ADDWISE">ADDWISE</option>
                                <option value="BANCHILE">BANCHILE INVERSIONES</option>
                                <option value="SHERPA">SHERPA</option>
                                <option value="SURA">SURA</option>
                            </select>

                            <label for="amount" class="form-label">¿Cuánto quieres invertir?</label>
                            <select name="" class="form-select" id="amount" name="amount" required>
                                <option value="" selected></option>
                                <option value="0 a $50.000.000">0 a $50.000.000</option>
                                <option value="$51.000.000 a $100.000.000">$51.000.000 a $100.000.000</option>
                                <option value="$101.000.000 a $150.000.000">$101.000.000 a $150.000.000</option>
                                <option value="más de $150.000.000">más de $150.000.000</option>
                            </select>
                        </div>
                        <div class="col-lg-3" data-aos="fade-up" data-aos-delay="300">
                            <label for="contacttype" class="form-label d-none">¿Cómo quieres ser contactado?</label>
                            <select multiple="multiple" class="w-100 selectBox d-none" name="contacttype" id="contacttype" required>
                                <option value="indiferente" selected>Me es indiferente</option>
                                <option value="telefono">Llamada telefónica</option>
                                <option value="whatsapp">Whatsapp</option>
                                <option value="email">Correo electrónico</option>
                            </select>

                            <label for="hour" class="form-label d-none">De lunes a viernes ¿En qué horario prefieres ser contactado?</label>
                            <select name="" class="form-select d-none" id="hour" name="hour" required>
                                <option value="indiferente" selected>Me es indiferente</option>
                                <option value="9:00 - 13:00">9:00 - 13:00</option>
                                <option value="14:00 - 18:00">14:00 - 18:00</option>
                                <option value="18:00 - 21:00">18:00 - 21:00</option>
                            </select>
                            <a class="btn" id="clear">Limpiar</a>
                            <button type="submit" class="btn enviar" id="send">Enviar</button>
                            <div class="text-center">
                                <div class="spinner-border text-secondary d-none mt-5" role="status" id="spinner">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>
                            
                            <p id="success">Mensaje enviado con éxito</p>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </section>

    <section class="section-10">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img src="/assets/img/logo-magna.png" alt="Nomina Magna Sequor" class="img-fluid" data-aos="fade-right">
                </div>
                <div class="col-lg-6" data-aos="fade-left">
                    <p>Tenemos una manera única de hacer las cosas.<br>Una perspectiva diferente para invertir que nos permite adaptarnos y desafiarnos constantemente, para entregar las mejores oportunidades de inversión, contribuyendo a la preservación y crecimiento patrimonial de nuestros clientes en el largo plazo.<br/>Nuestra filosofía de inversión está basada en el análisis fundamental con un foco en el largo plazo.<br/>
                        <strong>Activos bajo administración: USD 9.553 MM</strong>
                        </p>
                </div>
            </div>
        </div>
    </section>

    <section class="section-14 py-4 d-none">
        <div class="container d-flex align-items-center justify-content-between">
            <img src="/assets/img/circulos-4.png" alt="" class="circulos" data-aos="fade-right">
            <h4 data-aos="fade-up">¿Tienes dudas? Contacta a uno de nuestros expertos</h4>
            <a href="#" class="btn" data-aos="fade-down">Entrando aquí</a>
        </div>
    </section>

    <section class="section-15">
        <div class="container">
           <div id="legales" style="text-align: justify">
            <p><strong>Legales Campaña MRCLP:</strong><br/></p>

                <p>1)	Información de Fondos Mutuos obtenida desde la página de la Asociación de Fondos Mutuos para la rentabilidad del año 2021 en pesos para las categorías: 1. Fondos de Deuda >365 Días Nacional, Inversión en Pesos; 2. Fondos de Deuda >365 Días Nacional, Inversión en UF < 3 años; 3. Fondos de Deuda >365 Días Nacional, Inversión en UF > 3 años; 4. Fondos de Deuda >365 Días Nacional, Inversión en UF > 5 años; 5. Fondos de Deuda >365 Días Nacional, Inversión en UF > 3 años y 0<5; 6. Fondos de Deuda >365 Días Nacional</p>
                <p>Información de Fondos de Inversión Públicos vigentes el 31 de diciembre de 2021, que invierten en renta fija chilena, dentro de las categorías Rescatables y No Rescatables, Alternativos y Mobiliarios, de Inversión Directa, Deuda Pública y Privada, según la clasificación y registros de la Asociación Chilena Administradoras de Fondos de Inversión (ACAFI). Cálculo realizado en base al valor cuota de los fondos y los dividendos pagados, ambos obtenidos desde el sitio de la Comisión para el Mercado Financiero y la Bolsa de Comercio de Santiago.</p>


                <p>2)	La rentabilidad o ganancia obtenida en el pasado por este fondo, no garantiza que ella se repita en el futuro. Los valores de las cuotas de los fondos de inversión son variables.</p>

                <p>3)	Infórmese de las características esenciales de la inversión en este fondo de inversión, las que se encuentran contenidas en su reglamento interno.</p>

                <p>4)	Fondo con mayor rentabilidad anual compuesta, suponiendo reinversión de dividendos, de entre los Fondos de Inversión Públicos vigentes y existentes entre el 31 de diciembre de 2012 y 31 de diciembre de 2021, que invierten en renta fija chilena, dentro de las categorías Rescatables y No Rescatables, Alternativos y Mobiliarios, de Inversión Directa, Deuda Pública y Privada, según la clasificación y registros de la Asociación Chilena Administradoras de Fondos de Inversión (ACAFI). Cálculo realizado en base al valor cuota de los fondos y los dividendos pagados, ambos obtenidos desde el sitio de la Comisión para el Mercado Financiero y la Bolsa de Comercio de Santiago.</p>

                <p>5)	Dentro del grupo y periodo de tiempo considerado el fondo ocupó el primer lugar del ranking. Rendimiento corresponde al Yield To Maturity de la cartera de Moneda Renta CLP Fondo de Inversión, según información proporcionada al público en el Informe Mensual correspondiente a diciembre de 2021, el que consideras cifras al 31 de diciembre del 2021. El riesgo y retorno de las inversiones de Moneda Renta CLP Fondo de Inversión, así como su estructura de costos, no necesariamente se corresponden con aquellos de los referentes utilizados en la comparación. El riesgo y retorno de las inversiones de Moneda Renta CLP Fondo de Inversión, así como también su estructura de costos, no necesariamente se corresponden con las mismas variables de los otros fondos.</p>

                <p>6)	Las rentabilidades expuestas corresponden a la serie A del fondo al cierre del 31 de diciembre del 2021. La rentabilidad anualizada obtenida por el fondo, al mes de diciembre del 2021 corresponde a 14,6%, el último trimestre del 2021 a 3,5% y el último año a 14,6%. Dentro del grupo y periodo de tiempo antes mencionados, el fondo ocupó el primer lugar del ranking.</p>

                <p>7)	Fondo con mayor rentabilidad anual compuesta, suponiendo reinversión de dividendos, de entre los Fondos de Inversión Públicos vigentes y existentes entre el 31 de diciembre de 2012 y 31 de diciembre de 2021, que invierten en renta fija chilena, dentro de las categorías Rescatables y No Rescatables, Alternativos y Mobiliarios, de Inversión Directa, Deuda Pública y Privada, según la clasificación y registros de la Asociación Chilena Administradoras de Fondos de Inversión (ACAFI). Cálculo realizado en base al valor cuota de los fondos y los dividendos pagados, ambos obtenidos desde el sitio de la Comisión para el Mercado Financiero y la Bolsa de Comercio de Santiago.</p>

                <p>8)	$13.400, esta cifra corresponde al precio indicativo según el valor cuota del Fondo al 31 de diciembre de 2021 y publicado en el sitio web de la Comisión para el Mercado Financiero.</p>
           </div>
            <a href="#" id="vermas">ver más <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 16l-6-6h12z" fill="rgba(171,171,171,1)"/></svg></a>
        </div>
    </section>

    <footer>
        <div class="container">
            <nav class="d-flex justify-content-between" data-aos="fade-down">
                <li><a href="mailto:info@moneda.cl">info@moneda.cl</a></li>
                <li><a href="https://www.moneda.cl" target="_blank">moneda.cl</a></li>
                <li><a href="#">Síguenos en <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M18.335 18.339H15.67v-4.177c0-.996-.02-2.278-1.39-2.278-1.389 0-1.601 1.084-1.601 2.205v4.25h-2.666V9.75h2.56v1.17h.035c.358-.674 1.228-1.387 2.528-1.387 2.7 0 3.2 1.778 3.2 4.091v4.715zM7.003 8.575a1.546 1.546 0 0 1-1.548-1.549 1.548 1.548 0 1 1 1.547 1.549zm1.336 9.764H5.666V9.75H8.34v8.589zM19.67 3H4.329C3.593 3 3 3.58 3 4.297v15.406C3 20.42 3.594 21 4.328 21h15.338C20.4 21 21 20.42 21 19.703V4.297C21 3.58 20.4 3 19.666 3h.003z" fill="rgba(230,126,34,1)"/></svg></a></li>
            </nav>
        </div>
    </footer>


    <div id="float_box" class="float_box d-flex">
        <div class="float_box_content align-self-center">
            <img src="/assets/img/flecha-abajo-blanca.png" alt="Moneda" id="imgcontrae" role="button">
            <p>Invierte en<br>
Moneda Renta CLP<br>
a través de tu Corredora
de Bolsa: CFIMRCLPR</p>
            <a href="#contact" class="btn">
                Contáctanos
            </a>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.1.6/jquery.sumoselect.min.js"></script>
    <script src="/assets/js/bouncer.polyfills.min.js"></script>
    <script src="/assets/js/form.js?v=3.6"></script>
    <script src="/assets/js/aos.js"></script>
    <script src="/assets/js/main.js?v=3.6"></script>
</body>
</html>