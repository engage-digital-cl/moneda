<?php

session_start();
include_once("db-config.php");
$message = "";

if (isset($_POST['login'])) {
    $email    = $_POST['email'];
    $password = $_POST['password'];

    $result = mysqli_query($mysqli, "select 'email', 'password' from users_moneda
        where email='$email' and password='$password'");
    $user_matched = mysqli_num_rows($result);

    if ($user_matched > 0) {
        $_SESSION["email"] = $email;
        header("location: dashboard.php");
    } else {
        $message = '<div class="alert alert-primary" role="alert">Usuario o contraseña incorrectos.</div>';
    }
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneda Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3">
                <div class="card my-5">
                    <div class="card-body">
                        <img src="../assets/img/logo-moneda.png" alt="">
                        <form action="" method="post" name="form1" class="my-5">
                            <div class="mb-3">
                                <label for="email" class="form-label">Correo electrónico</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Contraseña</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <button type="submit" class="btn btn-primary" name="login" value="Login">Entrar</button>
                        </form>
                        <?php echo $message;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>