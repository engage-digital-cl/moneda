<?php
require('check.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneda Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php include('navbar.php');?>
    <div class="container">
        <div class="row">
            <main class="col-12">
                <div class="card m-5">
                    <div class="card-body">
                        <h1 class="h2">Usuarios</h1>
                        <?php
                        include_once("db-config.php");
                        $sql = "SELECT * FROM users_moneda";
                        $all_users = mysqli_query($mysqli, $sql);?>
                        <div class="w-100 text-end mb-4">
                            <a class="btn btn-primary" href="add-user.php">Agregar usuario</a>
                        </div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Compañía</th>
                                    <th class="text-end">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                while ($row = mysqli_fetch_assoc($all_users)){
                                    echo '<tr>
                                            <th scope="row">'.$row['id'].'</th>
                                            <td>'.$row['name'].'</td>
                                            <td>'.$row['email'].'</td>
                                            <td>'.$row['company'].'</td>
                                            <td class="text-end">
                                            <form method="post" action="edit-user.php">
                                                <input type="hidden" value="'.$row['id'].'" name="id"/>
                                                <button type="submit" class="btn btn-warning">Editar</button>
                                            </form>
                                            </td>
                                        </tr>';
                                }
                                $all_users->close();
                                
                                ?>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('.table').DataTable({
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/es-cl.json'
                }
            });
        } );
    </script>
</body>

</html>