<?php
require('check.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneda Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php include('navbar.php');?>
    <div class="container">
        <div class="row">
            <main class="col-12">
                <div class="card m-5">
                    <div class="card-body">
                        <?php
                            include_once("db-config.php");
                            $email = $_SESSION["email"];
                        if (!isset($_POST['delete']) && !isset($_POST['update'])) {
                            $current_id = $_POST["id"];
                            $current_user = mysqli_query($mysqli, "select * from users_moneda where id='$current_id'");
                            $current_user =  mysqli_fetch_assoc($current_user);
                            $current_name = $current_user['name'];
                            $current_password = $current_user['password'];
                            $current_email = $current_user['email'];
                            $current_company = $current_user['company'];
                            ?>
                        <h1 class="h2">Editar Usuario: <?php echo $current_name; ?></h1>
                        <form action="edit-user.php" method="post" name="form1">
                            <input type="hidden" name="id" value="<?php echo $current_id; ?>">
                            <div class="row">
                            <div class="mb-3 col-lg-6">
                                <label for="name" class="form-label">Nombre</label>
                                <input type="text" name="name" class="form-control" required value="<?php echo $current_name; ?>">
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" class="form-control" value="<?php echo $current_email; ?>"disabled>
                                <input type="hidden" value="<?php echo $current_email; ?>" name="email">
                                
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label for="password" class="form-label">Contraseña</label>
                                <input type="password" name="password" class="form-control" value="<?php echo $current_password; ?>"required>
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label for="company" class="form-label">Distribuidor</label>
                                
                                <select name="company" id="company" class="form-select" required>
                                    <option value="">Selecciona un distribuidor</option>
                                    <option value="ADDWISE" <?php echo ($current_company=='ADDWISE'?'selected':'');?>>ADDWISE</option>
                                    <option value="BANCHILE" <?php echo ($current_company=='BANCHILE'?'selected':'');?>>BANCHILE INVERSIONES</option>
                                    <option value="SHERPA" <?php echo ($current_company=='SHERPA'?'selected':'');?>>SHERPA</option>
                                    <option value="SURA" <?php echo ($current_company=='SURA'?'selected':'');?>>SURA</option>
                                    <option value="ADMIN" <?php echo ($current_company=='ADMIN'?'selected':'');?>>Administrador</option>
                                </select>
                            </div>
                            <div class=" mb-3">
                                <input type="submit" name="update" value="Guardar cambios" class="btn btn-primary">
                                <input type="button" value="Eliminar" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal">
                            </div>
                            <?php
                            }
                            if (isset($_POST['update'])) {
                                $current_name     = $_POST['name'];
                                $current_email    = $_POST['email'];
                                $current_password = $_POST['password'];
                                $current_company = $_POST['company'];
                                $current_id = $_POST["id"];

                                $email_result = mysqli_query($mysqli, "UPDATE users_moneda SET name='$current_name', password='$current_password', company='$current_company' WHERE email='$current_email'");
                                header( "refresh:3;url=users.php" );                            
                                echo '<div class="alert alert-success" role="alert">
                                            Se ha actualizado el usuario. Redireccionando en 3 segundos.
                                        </div>';
                            }
                            if(isset($_POST['delete'])){
                                $user_id     = $_POST['userid'];
                                $delete_query = mysqli_query($mysqli, "DELETE FROM users_moneda where id='$user_id'");
                                
                            }

                            ?>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>
<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLabel">¿Desea eliminar el usuario?</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form action="" method="post" >
        <div class="modal-body">
           <input type="hidden" name="userid" value="<?php echo $current_id;?>">
           <p>Esta acción no se puede deshacer.<br/>
        ¿Está seguro?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            <input type="submit" name="delete" value="Eliminar" class="btn btn-danger">
        </div>
      </form>
    </div>
  </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>