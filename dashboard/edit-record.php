<?php
require('check.php');
include_once("db-config.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require '../PHPMailer/src/Exception.php';
require '../PHPMailer/src/PHPMailer.php';
require '../PHPMailer/src/SMTP.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneda Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php include('navbar.php');?>
    <div class="container">
        <div class="row">
            <main class="col-12">
                <div class="card m-5">
                    <div class="card-body">
                        <?php
                            
                            $email = $_SESSION["email"];
                            if (!isset($_POST['update'])):
                                $current_id = $_POST["id"];
                                $current_record = mysqli_query($mysqli, "select * from contact_form_info where id='$current_id'");
                                $current_record =  mysqli_fetch_assoc($current_record);
                                $current_name = $current_record['name'];
                                $current_lastname = $current_record['lastname'];
                                $current_email = $current_record['email'];
                                $current_phone = $current_record['phone'];
                                $current_company = $current_record['distributor'];
                                $current_amount = $current_record['amount'];
                                $current_contact = $current_record['contact'];
                                $current_date = $current_record['date'];
                                $current_hour = $current_record['hour'];
                                $current_comments = $current_record['comments'];
                                $current_created = $current_record['created'];
                                $current_status = $current_record['status'];

                            ?>
                        <h1 class="h2">Registro de: <?php echo $current_name; ?></h1>
                        
                        <form action="" method="post" name="form1">
                            <div class="row">
                                <input type="hidden" name="record_id" value="<?php echo $current_id;?>">
                                <input type="hidden" name="record_email" value="<?php echo $current_email; ?>">
                                <input type="hidden" name="record_distributor" value="<?php echo $current_company; ?>">
                                <div class="mb-3 col-6">
                                    <label for="name" class="form-label">Nombre</label>
                                    <input type="text" name="name" class="form-control" required value="<?php echo $current_name; ?>" disabled>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" name="email" class="form-control" value="<?php echo $current_email; ?>" disabled>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="lastname" class="form-label">Apellido</label>
                                    <input type="text" name="lastname" class="form-control" value="<?php echo $current_lastname; ?>" disabled>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="phone" class="form-label">Teléfono</label>
                                    <input type="text" name="phone" class="form-control" value="<?php echo $current_phone; ?>" disabled>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="company" class="form-label">Distribuidor</label>
                                    <input type="text" name="lastname" class="form-control" value="<?php echo $current_company; ?>" disabled>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="company" class="form-label">Cantidad a invertir</label>
                                    <input type="text" name="lastname" class="form-control" value="<?php echo $current_amount; ?>" disabled>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="company" class="form-label">Tipo de contacto de preferencia</label>
                                    <input type="text" name="lastname" class="form-control" value="<?php echo $current_contact; ?>" disabled>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="company" class="form-label">Horario de contacto</label>
                                    <input type="text" name="lastname" class="form-control" value="<?php echo $current_hour; ?>" disabled>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="company" class="form-label">Fecha de registro</label>
                                    <input type="text" name="lastname" class="form-control" value="<?php echo $current_created; ?>" disabled>
                                </div>

                                <hr>
                                <h2 class="h3 mb-3">Opciones</h2>
                                <div class="mb-3 col-3">
                                    <label for="status" class="form-label">Estado:</label>
                                    <select name="status" id="status" class="form-select">
                                        <option value="0" <?php echo ($current_status=='0'?'selected':'');?>>Sin actividad</option>
                                        <option value="1" <?php echo ($current_status=='1'?'selected':'');?>>Contactado</option>
                                        <option value="2" <?php echo ($current_status=='2'?'selected':'');?>>No contactado</option>
                                        <option value="3" <?php echo ($current_status=='3'?'selected':'');?>>Cliente</option>
                                        <option value="4" <?php echo ($current_status=='4'?'selected':'');?>>No cliente</option>
                                        <option value="5" <?php echo ($current_status=='5'?'selected':'');?>>Inicio negociación</option>
                                        <option value="6" <?php echo ($current_status=='6'?'selected':'');?>>En negociación</option>
                                        <option value="7" <?php echo ($current_status=='7'?'selected':'');?>>Abandono del proceso</option>
                                        <option value="8" <?php echo ($current_status=='8'?'selected':'');?>>Cierre de negociación</option>
                                        <option value="9" <?php echo ($current_status=='9'?'selected':'');?>>Cliente no cumple</option>
                                    </select>
                                    <div class="alert alert-warning mt-4 d-none" role="alert" id="alert">
                                        <strong>Advertencia:</strong> Al guardar los cambios se le enviará un correo al contacto.
                                    </div>
                                </div>
                                <div class="mb-3 col-9">
                                    <label for="company" class="form-label">Comentarios interno</label>
                                    <textarea name="comments" id="" cols="20" rows="6" class="form-control"><?php echo $current_comments; ?></textarea>
                                </div>
                                
                                <div class="col-12 my-3 text-end">
                                    <input type="submit" name="update" value="Guardar cambios" class="btn btn-primary">
                                    <a href="dashboard.php" class="btn btn-warning">Volver</a>
                                </div>
                                <?php else: 
                                    
                                    include_once("db-config.php");

                                    $current_id = $_POST['record_id'];
                                    $current_status = $_POST['status'];
                                    $current_comments = $_POST['comments'];
                                    $current_email = $_POST['record_email'];
                                    $current_distributor = $_POST['record_distributor'];

                                    function sendMail($destination, $mailBody, $Host, $User, $Pass){
                                        //new mail notificación
                                        $mail = new PHPMailer(true);
                                        try {
                                            //Server settings
                                            $mail->isSMTP();                                            //Send using SMTP
                                            $mail->Host       = $Host;                     //Set the SMTP server to send through
                                            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                                            $mail->Username   = $User;                     //SMTP username
                                            $mail->Password   =  $Pass;                               //SMTP password
                                            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
                                            $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
                                        
                                            //Recipients
                                            $mail->setFrom($User, 'Contacto Moneda Renta CLP');
                                            $mail->addAddress($destination);     //Add a recipient
                                            $mail->addReplyTo($User, 'Contacto Moneda Renta CLP');
                                        
                                            //Content
                                            $mail->isHTML(true);                                  //Set email format to HTML
                                            $mail->CharSet = 'UTF-8';
                                            $mail->Subject = 'Notificación Moneda Renta CLP';
                                            $mail->Body    = $mailBody;
                                            $mail->AltBody = $mailBody;
                                        
                                            $mail->send();

                                        } catch (Exception $e) {
                                            echo "Error: {$mail->ErrorInfo}";
                                        }
                                    }

                                    $body_cliente = '<p>Felicitaciones, '.$current_distributor.' te da la bienvenida!</p>';
                                    $body_no_cliente = '<p>Lo sentimos, no has sido seleccionado.<br/>Puedes volver a contactarnos en el futuro.</p>';

                                    $update_record = mysqli_query($mysqli, "UPDATE contact_form_info SET comments='$current_comments', status='$current_status' WHERE id='$current_id'");
                                    
                                    $msg = '';

                                    switch ($current_status) {
                                        case "0":
                                            $msg = "Se han guardado los cambios. Estado <strong>SIN ACTIVIDAD</strong>";
                                            break;
                                        case "1":
                                            $msg = "Se ha cambiado a <strong>CONTACTADO</strong>";
                                            break;
                                        case "2":
                                            $msg = "Se ha cambiado a <strong>NO CONTACTADO</strong>";
                                            break;
                                        case "3":
                                            $msg = "Se ha cambiado a <strong>CLIENTE</strong>.<p>Se envió un correo a ".$current_email.".</p>";
                                            sendMail($current_email, $body_cliente, $fromEmailHost, $fromEmailUser, $fromEmailPass);
                                            break;
                                        case "4":
                                            $msg = "Se ha cambiado a <strong>NO CLIENTE</strong>.<p>Se envió un correo a ".$current_email.".</p>";
                                            sendMail($current_email, $body_no_cliente, $fromEmailHost, $fromEmailUser, $fromEmailPass);
                                            break;
                                        case "5":
                                            $msg = "Se ha cambiado a <strong>INICIO DE NEGOCIACIÓN</strong>.";
                                            break;
                                        case "6":
                                            $msg = "Se ha cambiado a <strong>EN NEGOCIACIÓN</strong>.";
                                            break;
                                        case "7":
                                            $msg = "Se ha cambiado a <strong>ABANDONO DEL PROCESO</strong>.";
                                            break;
                                        case "8":
                                            $msg = "Se ha cambiado a <strong>CIERRE DE NEGOCIACIÓN</strong>.";
                                            break;
                                        case "9":
                                            $msg = "Se ha cambiado a <strong>CLIENTE NO CUMPLE</strong>.";
                                            break;
                                        default:
                                            $msg = "Ha ocurrido un error";
                                    }
                                    header( "refresh:5;url=dashboard.php" );                            
                                    echo '<h3 class="my-3">Modificación del registro: ID'.$current_id.'</h3>
                                            <div class="alert alert-success" role="alert">
                                                '.$msg.'
                                          </div>';
                                    if ($current_comments) {
                                        echo '<p>Con el siguiente comentario: <strong>'.$current_comments.'</strong></p>';
                                    }
                                    echo '<pre class="text-end">Redireccionando en 5 segundos.</pre>';
                                
                                endif;?>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>

<script>
    let statusSelector = document.getElementById('status')
    let alerta = document.getElementById('alert')
    statusSelector.addEventListener('change', (e)=>{
        
        if (statusSelector.value == '3' || statusSelector.value == '4') {
            alerta.classList.remove('d-none')
        }
        else{
            alerta.classList.add('d-none')
        }
    })
</script>
</body>

</html>