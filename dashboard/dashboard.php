<?php
require('check.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneda Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.1.0/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php include('navbar.php');?>
    <div class="container-fluid">
        <div class="row">
            <main class="col-12">
                <div class="card m-5">
                    <div class="card-body">
                        <h1 class="h2 mb-4">Registros</h1>
                        <?php
                        include_once("db-config.php");
                        if ($active_company == 'ADMIN') {
                            $sql = "SELECT * FROM contact_form_info";
                        } else {
                            $sql = 'SELECT * FROM contact_form_info WHERE distributor="'.$active_company.'"';
                        }
                        
                        $all_records = mysqli_query($mysqli, $sql);?>
                        <table class="table table-striped" data-order='[[ 5, "desc" ]]'>
                            <thead>
                                <tr>
                                    <th scope="col">Estado</th>
                                    <th scope="col">Nombre completo</th>
                                    <th scope="col">Teléfono</th>
                                    <th scope="col">EMail</th>
                                    <th scope="col">Cantidad</th>
                                    <th scope="col">Creado</th>
                                    <th scope="col">Comentarios</th>
                                    <th class="text-end">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                while ($row = mysqli_fetch_assoc($all_records)){
                                    echo '<tr>
                                            <td>';
                                        echo ($row['status']=='0'?'Sin actividad':'');
                                        echo ($row['status']=='1'?'Contactado':'');
                                        echo ($row['status']=='2'?'No contactado':'');
                                        echo ($row['status']=='3'?'Cliente':'');
                                        echo ($row['status']=='4'?'No cliente':'');
                                        echo ($row['status']=='5'?'Inicio negociación':'');
                                        echo ($row['status']=='6'?'En negociación':'');
                                        echo ($row['status']=='7'?'Abandono del proceso':'');
                                        echo ($row['status']=='8'?'Cierre de negociación':'');
                                        echo ($row['status']=='9'?'Cliente no cumple':'');
                                    echo '  </td>
                                            <td>'.$row['name'].' '.$row['lastname'].'</td>
                                            <td>'.$row['phone'].'</td>
                                            <td>'.$row['email'].'</td>
                                            <td>'.$row['amount'].'</td>
                                            <td>'.$row['created'].'</td>
                                            <td>'.$row['comments'].'</td>
                                            <td class="text-end">
                                            <form method="post" action="edit-record.php">
                                                <input type="hidden" value="'.$row['id'].'" name="id"/>
                                                <button type="submit" class="btn btn-warning">Ver registro</button>
                                            </form>
                                            </td>
                                        </tr>';
                                }
                                $all_records->close();
                                
                                ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.1.0/js/buttons.html5.min.js"></script>
    <script>
        $(document).ready( function () {
            $('.table').DataTable({
                "scrollX": true,
                language: {
                    url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/es-cl.json'
                },
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'excel'
                ]
            });
        } );
    </script>
    <style>
        table.table-striped.dataTable.no-footer{
            width: 100% !important
        }
        button.dt-button, div.dt-button, a.dt-button, input.dt-button {
            background-color: #486DCF !important;
            color: white;
            display: inline-block;
            font-weight: 400;
            line-height: 1.5;
            text-align: center;
            text-decoration: none;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            border-color: #486DCF;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            border-radius: 0.25rem;
            transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
        }

    </style>
</body>

</html>