<?php
require('check.php');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneda Dashboard</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<?php include('navbar.php');?>
    <div class="container">
        <div class="row">
            <main class="col-12">
                <div class="card m-5">
                    <div class="card-body">
                        <h1 class="h2">Crear nuevo usuario</h1>
                        <form action="add-user.php" method="post" name="form1">
                            <div class="row">
                            <div class="mb-3 col-lg-6">
                                <label for="name" class="form-label">Nombre</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label for="email" class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" required>
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label for="password" class="form-label">Contraseña</label>
                                <input type="password" name="password" class="form-control" required>
                            </div>
                            <div class="mb-3 col-lg-6">
                                <label for="company" class="form-label">Empresa</label>
                                <select name="company" id="company" class="form-select" required>
                                    <option value="">Selecciona una empresa</option>
                                    <option value="ADDWISE">ADDWISE</option>
                                    <option value="BANCHILE">BANCHILE INVERSIONES</option>
                                    <option value="SHERPA">SHERPA</option>
                                    <option value="SURA">SURA</option>
                                    <option value="ADMIN">--ADMINISTRADOR DEL SISTEMA</option>
                                </select>
                            </div>
                            <div class=" mb-3">
                                <input type="submit" name="register" value="Agregar usuario" class="btn btn-primary">
                            </div>
                            <?php

                            include_once("db-config.php");

                            if (isset($_POST['register'])) {
                                $name     = $_POST['name'];
                                $email    = $_POST['email'];
                                $password = $_POST['password'];
                                $company = $_POST['company'];

                                $email_result = mysqli_query($mysqli, "select 'email' from users_moneda where email='$email' and password='$password'");
                                $user_matched = mysqli_num_rows($email_result);
                                if ($user_matched > 0) {
                                    echo '<div class="alert alert-danger" role="alert">
                                        Ya existe el usuario: '.$email.'
                                      </div>' ;
                                } else {
                                    $result   = mysqli_query($mysqli, "INSERT INTO users_moneda(name,email,password, company) VALUES('$name','$email','$password', '$company')");

                                    if ($result) {
                                        echo '<div class="alert alert-success" role="alert">
                                                    Usuario creado con éxito
                                                </div>';
                                    } else {
                                        echo '<div class="alert alert-danger" role="alert">
                                        Error al intentar crear el usuario: '.mysqli_error($mysqli).'
                                      </div>' ;
                                    }
                                }
                            }

                            ?>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>

</body>

</html>