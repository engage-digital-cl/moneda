<?php date_default_timezone_set("America/Santiago");?>
<nav class="navbar navbar-expand-lg navbar-light shadow-xs">
  <div class="container">
    <a class="navbar-brand" href="dashboard.php">
        <img src="../assets/img/logo-moneda.png" alt="" width="100">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="dashboard.php">Registros</a>
        </li>
        <?php if ($active_company == 'ADMIN'):?>
        <li class="nav-item">
          <a class="nav-link" href="users.php">Usuarios</a>
        </li>
        <?php endif;?>
      </ul>
      <ul class="navbar-nav">
        <li class="nav-item">
          <p class="my-2">Bienvenido: <?php echo $active_name;?> (<?php echo $active_company;?>) | </p>
        </li>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="logout.php">Salir</a>
        </li>
      </ul>
    </div>
  </div>
</nav>