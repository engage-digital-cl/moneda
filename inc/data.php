<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require '../PHPMailer/src/Exception.php';
require '../PHPMailer/src/PHPMailer.php';
require '../PHPMailer/src/SMTP.php';
require_once("config.php");

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '. gmdate('D, d M Y H:i:s') . 'GMT');
header('Cache-Control: no-cache, must-revalidate');
header('Pragma: no-cache');
header('Content-type: application/json'); 
date_default_timezone_set("America/Santiago");

$_POST = json_decode(file_get_contents("php://input"), true);


if((isset($_POST['name'])&& $_POST['name'] !='') && (isset($_POST['email'])&& $_POST['email'] !='')){
    
    $name = $conn->real_escape_string($_POST['name']);
    $lastname = $conn->real_escape_string($_POST['lastname']);
    $email = $conn->real_escape_string($_POST['email']);
    $phone = $conn->real_escape_string($_POST['phone']);
    $distributor = $conn->real_escape_string($_POST['distributor']);
    $amount = $conn->real_escape_string($_POST['amount']);
    $contact = $conn->real_escape_string($_POST['contact']);
    $hour = $conn->real_escape_string($_POST['hour']);

    $sql="INSERT INTO contact_form_info (name, lastname, email, phone, distributor, amount, contact, hour) VALUES ('".$name."','".$lastname."', '".$email."', '".$phone."', '".$distributor."', '".$amount."', '".$contact."', '".$hour."')";
    
    if(!$result = $conn->query($sql)){
        echo json_encode('Error [' . $conn->error . ']');
    } else {
        $toEmail = "moneda@moneda.cl";
        $fromEmail = $fromEmailUser;
        $mailBody = 'Nombre: '.$name.'<br/> Apellido: '.$lastname.'<br/> Email: '.$email.'<br/> Teléfono: '.$phone.'<br/> Distribuidor: '.$distributor.'<br/> Cantidad a invertir: '.$amount.'<br/> Tipo de contacto: '.$contact.'<br/> Horario: '.$hour;
        
        //new mail notificación
        $mail = new PHPMailer(true);
        try {
            //Server settings
           // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = $fromEmailHost;                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = $fromEmailUser;                     //SMTP username
            $mail->Password   =  $fromEmailPass;                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
        
            //Recipients
            $mail->setFrom($fromEmailUser, 'Contacto Moneda Renta CLP');
            $mail->addAddress($toEmail);     //Add a recipient
            $mail->addReplyTo($fromEmailUser, 'Contacto Moneda Renta CLP');
        
            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->CharSet = 'UTF-8';
            $mail->Subject = 'Nuevo registro Moneda Renta CLP';
            $mail->Body    = $mailBody;
            $mail->AltBody = $mailBody;
        
            $mail->send();

        } catch (Exception $e) {
            echo "Error: {$mail->ErrorInfo}";
        }
        //mail interesado
        $mail2 = new PHPMailer(true);
        try {
            //Server settings
            $mail2->isSMTP();                                            //Send using SMTP
            $mail->Host       = $fromEmailHost;                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = $fromEmailUser;                     //SMTP username
            $mail->Password   =  $fromEmailPass;                                //SMTP password
            $mail2->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail2->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
        
            //Recipients
            $mail2->setFrom($fromEmailUser, 'Contacto Moneda Renta CLP');
            $mail2->addAddress($email);     //Add a recipient
            $mail2->addReplyTo($fromEmailUser, 'Contacto Moneda Renta CLP');
        
            //Content
            $mail2->isHTML(true);                                  //Set email format to HTML
            $mail2->CharSet = 'UTF-8';
            $mail2->Subject = 'Información enviada a Moneda Renta CLP';
            $mail2->Body    = $name.', tu información ha sido enviada con éxito. <br/>Pronto nos contactaremos contigo.';
            $mail2->AltBody = $name.', tu información ha sido enviada con éxito. <br/>Pronto nos contactaremos contigo.';
        
            $mail2->send();                        
            
            
        } catch (Exception $e) {
           
        }
        echo json_encode('success');        
    }
 } else {
    echo json_encode('ERROR');
 }

 ?>