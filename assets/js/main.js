function number_format(amount, decimals) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));
    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);
    amount = '' + amount.toFixed(decimals);
    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;
    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + '.' + '$2');
    return amount_parts.join('.');
}

$(document).ready(function () {
    //format phone number
    $('#phone').inputmask('(+56)999999999');
    //Select multiple
    $('.selectBox').SumoSelect({
        placeholder: 'Elige una forma de contacto',
        okCancelInMulti: false,
        csvDispCount: 6,
        forceCustomRendering: true,
        triggerChangeCombined: false,
        captionFormat: '{0} seleccionados',
        captionFormatAllSelected: '{0} seleccionados'
    });
});
//process data 
function sendForm() {
    let spinner = document.getElementById('spinner')
    let data = new FormData()
    let contactType = ""
    spinner.classList.remove('d-none')

    Array.from(document.querySelector("#contacttype").options).forEach(function (option_element) {
        let option_text = option_element.text;
        let is_option_selected = option_element.selected;
        if (is_option_selected === true) {
            contactType = `${contactType} ${option_text},`
        }
    });

    data = {
        'name': document.getElementById('name').value,
        'lastname': document.getElementById('lastname').value,
        'phone': document.getElementById('phone').value,
        'email': document.getElementById('email').value,
        'distributor': document.getElementById('distributor').value,
        'amount': document.getElementById('amount').value,
        'contact': contactType.slice(0, -1),
        'hour': document.getElementById('hour').value
    }

    fetch('/inc/data.php', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(data => {
            if (data == 'success') {
                spinner.classList.add('d-none')
                clearForm()
                document.getElementById('success').style.display = 'block'
                setTimeout(() => {
                    document.getElementById('success').style.display = 'none'
                }, 5000)

                window.location.href = "https://www.monedarentaclp.cl/gracias.php";
            }
        });

}

//calc
let monto = document.querySelector('.inputmonto')
monto.addEventListener('input', () => {
    document.querySelectorAll('.calc2013').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 1.10233511116644)}` })
    document.querySelectorAll('.calc2014').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 1.25972698099039)}` })
    document.querySelectorAll('.calc2015').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 1.33014773631766)}` })
    document.querySelectorAll('.calc2016').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 1.5511750920528)}` })
    document.querySelectorAll('.calc2017').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 1.73851663929797)}` })
    document.querySelectorAll('.calc2018').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 1.88001730117829)}` })
    document.querySelectorAll('.calc2019').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 1.90736398727662)}` })
    document.querySelectorAll('.calc2020').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 1.91071787703657)}` })
    document.querySelectorAll('.calc2021').forEach(function (el) { el.innerHTML = `$${number_format(monto.value * 2.18987375887161)}` })
})

//clear form
function clearForm() {
    let inputElements = document.getElementsByTagName("input")
    let selectElements = document.getElementsByTagName("select")
    document.getElementById('fieldset').removeAttribute('disabled')
    for (var ii = 0; ii < inputElements.length; ii++) {
        inputElements[ii].value = ""
    }
    for (var ii = 0; ii < selectElements.length; ii++) {
        selectElements[ii].value = ""
    }
}
document.getElementById('clear').addEventListener('click', () => {
    clearForm()
})

//on valid form
document.addEventListener('bouncerFormValid', function (event) {
    document.getElementById('fieldset').setAttribute('disabled', 'disabled')
    sendForm()
}, false);

//AOS init
AOS.init();

//anim floating box
setTimeout(function () {
    document.getElementById('float_box').classList.add("visible")
}, 2000);
document.getElementById('imgcontrae').addEventListener('click', (e) => {
    e.preventDefault()
    document.getElementById('float_box').classList.remove("visible")
})


//show legales
document.getElementById('vermas').addEventListener('click', (e) => {
    e.preventDefault()
    document.getElementById('legales').style.height = 'auto'
    document.getElementById('vermas').style.display = 'none'
})