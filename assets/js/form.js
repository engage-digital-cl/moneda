var bouncer = new Bouncer('[data-validate]',{
    messageAfterField: true,
    messageCustom: 'data-bouncer-message',
    messageTarget: 'data-bouncer-target',
    messages: {
      missingValue: {
        checkbox: 'Este campo es requerido.',
        select: 'Selecciona una opción.',
        'select-multiple': 'Selecciona por lo menos una opción.',
        default: 'Campo requerido.'
      },
      patternMismatch: {
        email: 'Ingresa una dirección de correo válida.',
        number: 'Debes ingresar un número',
        default: 'Ingresa el formato correcto.'
      },
      outOfRange: {
        over: 'El campo no puede ser mayor a {max}.',
        under: 'El campo no puede ser menor a {min}.'
      },
      wrongLength: {
        under: 'Debes ingresar al menos {minLength} caracteres.'
      },
      fallback: 'Ha ocurrido un problema con este campo.'
    },
    patterns: {
        email: /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*(\.\w{2,})+$/,
    },
    disableSubmit: true,
    emitEvents: true
})