<?php	
	@ini_set('expose_php', 'off');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Moneda Renta CLP Fondo de Inversión.</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.sumoselect/3.1.6/sumoselect.min.css">
    <link rel="stylesheet" href="assets/fonts/Hatton.css">
    <link rel="stylesheet" href="assets/css/main.css?v=6">
    <link rel="icon" type="image/png" href="favicon.png">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-P9QH2HS');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9QH2HS"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <nav class="navbar navbar-light">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="/assets/img/logo-moneda.png" alt="">
            </a>
            <!--<a href="#">
            <img src="/assets/img/menu-icon.png" alt="">
            </a>-->
        </div>
    </nav>
    

    <section class="section-3 p-0">
        <div class="cifras gracias">
            <div class="container">
                <div class="row g-0">
                    <div class="col-12 text-center">
                        <h2>¡Muchas gracias!</h2>
                        <p>Tus datos han sido ingresados con éxito, pronto podrás invertir en Moneda Renta CLP.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>  

    <footer>
        <div class="container">
            <nav class="d-flex justify-content-between">
                <li><a href="mailto:info@moneda.cl">info@moneda.cl</a></li>
                <li><a href="https://www.moneda.cl" target="_blank">moneda.cl</a></li>
                <li><a href="#">Síguenos en <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M18.335 18.339H15.67v-4.177c0-.996-.02-2.278-1.39-2.278-1.389 0-1.601 1.084-1.601 2.205v4.25h-2.666V9.75h2.56v1.17h.035c.358-.674 1.228-1.387 2.528-1.387 2.7 0 3.2 1.778 3.2 4.091v4.715zM7.003 8.575a1.546 1.546 0 0 1-1.548-1.549 1.548 1.548 0 1 1 1.547 1.549zm1.336 9.764H5.666V9.75H8.34v8.589zM19.67 3H4.329C3.593 3 3 3.58 3 4.297v15.406C3 20.42 3.594 21 4.328 21h15.338C20.4 21 21 20.42 21 19.703V4.297C21 3.58 20.4 3 19.666 3h.003z" fill="rgba(230,126,34,1)"/></svg></a></li>
            </nav>
        </div>
    </footer>
</body>
</html>